from django.urls import path
from . import views

app_name = 'cookie'

urlpatterns = [
    path('login', views.login_user , name ='login'),
    path('', views.loginFinish , name ='loginFinish'),
    path('logout', views.logout_user, name='logout'),
    path('signup', views.signup, name='signup'),
]
