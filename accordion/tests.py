from django.test import TestCase, Client
from . import views
# Create your tests here.
class AccordionTest(TestCase):
    def test_accordion_url(self):
        response = Client().get('/accordion')
        self.assertEqual(response.status_code, 200)
    def test_accordion_html(self):
        response = Client().get('/accordion')
        self.assertTemplateUsed(response, "accordionpage.html")

