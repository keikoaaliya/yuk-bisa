from django.urls import path

from . import views

app_name = 'ajax'

urlpatterns = [
    path('book', views.halaman, name='halaman'),
    path('dataBuku', views.dataBuku),
]