from django.test import TestCase

# Create your tests here.
from django.test import TestCase, Client
from django.urls import resolve
from .views import halaman
from .views import dataBuku
# Create your tests here.
class Lab8UnitTest(TestCase):
    def test_lab8_ada_url(self):
        response = Client().get('/book')
        self.assertEqual(response.status_code, 200)

    def test_lab8_ada_func_halaman(self):
        found = resolve('/book')
        self.assertEqual(found.func, halaman)
    
    def test_lab8_ada_template(self):
        response = Client().get('/book')
        self.assertTemplateUsed(response, 'book.html')

    def test_lab8_ada_button_search_atau_ga(self):
        response = Client().get('/book')
        self.assertContains(response, '</button>')
