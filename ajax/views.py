from django.shortcuts import render
from django.http import JsonResponse
from django.http import HttpResponse
import requests

# Create your views here.
def halaman(request):
    return render(request,'book.html')

def dataBuku(request):
    key = request.GET['key']
    url = 'https://www.googleapis.com/books/v1/volumes?q=' + key
    response = requests.get(url)
    response_json = response.json()

    return JsonResponse(response_json)